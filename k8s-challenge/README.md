# How to test challenge 4
-----------------------------
- [How to use a local registry](https://microk8s.io/docs/registry-images)

    - ```sudo docker build . -t nodejs-api-rest:local```
    - ```sudo docker save nodejs-api-rest > nodejs-api-rest.tar```
    - ```sudo microk8s ctr image import nodejs-api-rest.tar```
    - ```sudo microk8s ctr images ls```

- Finally:
    - ```sudo microk8s kubectl apply -f api-nodejs-deployment.yaml```

- Expose a service:
    - ```sudo microk8s kubectl expose deployment api-deployment --type=NodePort --name=api-rest-service```

- List pods that are running the nodejs API:
    - ```sudo microk8s kubectl get pods --selector="app=api-rest" --output=wide```

- To see the public-node-ip:
    - ```sudo microk8s kubectl cluster-info dump```

- Testing the API:
    - ```curl http://<public-node-ip>:<node-port>```
    - ```curl http://<public-node-ip>:<node-port>/private```
    - Ex: ```curl http://192.168.100.220:30011/private```

- To delete the service:
    - ```sudo microk8s kubectl delete services api-rest-service```

- To Delete the deployment:
    - ```sudo microk8s kubectl delete deployment api-deployment```

- Test HPA:

    - until $(curl http://192.168.100.220:30015/private); do
        printf '.'
        sleep 0.01
      done

until $(curl http://192.168.100.220:30015/); do
    sleep 0.01
done

```sudo microk8s kubectl get hpa api-rest-hpa --watch```
