# helm challenge
## Architecture
![Architecture](./diagram/helm-architecture.png)
## Instructions (tested in microk8s)
- Be sure your helm3 and ingress plugins are enabled.
- Add the repo: ```microk8s helm3 repo add darwin-repo https://djvr-docs-2021.s3.amazonaws.com/helm-challenge/```
- Show values you can customize (optional): ```microk8s helm3 show values darwin-repo/my-api-chart```
- Install the chart. Example: ```microk8s helm3 install myapinodejs darwin-repo/my-api-chart```