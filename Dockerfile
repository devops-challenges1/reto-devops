# Slim image for reduce size
FROM node:10-alpine
# create directory and add permision to node user for easier app bind mounting
RUN mkdir -p /home/node/app && chown -R node:node /home/node/app
WORKDIR /home/node/app
# the official node image provides an unprivileged user as a security best practice
# but we have to manually enable it. We put it here so npm installs dependencies as the same
# user who runs the app.
# https://github.com/nodejs/docker-node/blob/master/docs/BestPractices.md#non-root-user
USER node
COPY --chown=node:node package.json package-lock.json index.js ./
# install dependencies
RUN npm install --silent
# when we create a container exposes the app on port 3000
EXPOSE 3000
CMD ["node", "index.js"]
# https://github.com/nodejs/docker-node/blob/main/docs/BestPractices.md#non-root-user
# At the end, set the user to use when running this image
USER node
